# OpenConnect Configuration #

Reason: AnyConnect bugs. And,

    https://developer.apple.com/forums/thread/117603

### Where it is? ###

http://www.infradead.org/openconnect/index.html
https://gitlab.com/openconnect/openconnect

### How do I get set up? ###

MacOS:

    brew install openconnect
    
Ubuntu/Debian:

    sudo apt-get install openconnect

install vpnc-script per instructions here:

    http://www.infradead.org/openconnect/vpnc-script.html

### How to connect

CAVEAT: Before you connect, you will be asked your local sudo password
If you are not comfortable with running this as root, exit now by 
entering ^C.

However, if sudo is already active from the recent authentication, no warning will be issued

In the project root directory, run

    ./connect

### How to disconnect

In the project root directory, run

    ./disconnect

### How to reconnect 

Use if IP address changed

    ./reconnect 

### Future development

Add launchd scripts using LaunchControl:

    https://www.soma-zone.com/LaunchControl/
