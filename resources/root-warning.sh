if sudo -n true 2>/dev/null; then
    printf "+\n"
else
    echo 'CAVEAT: if you see this when you try to connect, disconnect or reconnect,'
    echo "you need to provide sudo password for the user '${USER}',"
    echo 'which might be different from the password you use for VPN.'
    echo 'If you are not comfortable with running this as root,'
    echo 'exit now by simultaneously pressing Control+C'
    printf '\n'
fi
